##################################################################################
# VARIABLES
##################################################################################

# Credentials
variable "vsphere_server" {
  type        = string
  description = "The fully qualified domain name or IP address of the vCenter Server instance. (e.g. sfo-m01-vc01.sfo.rainpole.io)"
}

variable "vsphere_username" {
  type        = string
  description = "The username to login to the vCenter Server instance. (e.g. administrator@vsphere.local)"
  sensitive   = true
}

variable "vsphere_password" {
  type        = string
  description = "The password for the login to the vCenter Server instance."
  sensitive   = true
}

variable "vsphere_insecure" {
  type        = bool
  description = "Set to true for self-signed certificates."
  default     = false
}

# vCenter Settings
variable "vsphere_datacenter" {
  description = "The name of the datacenter. This can be a name or path. Can be omitted if there is only one datacenter in the inventory"
  type        = string
}

variable "vsphere_cluster" {
  description = "The name or absolute path to the cluster"
  type        = string
}

variable "vsphere_datastore" {
  description = "The name of the datastore. This can be a name or path"
  type        = string
}

variable "vsphere_folder" {
  description = "The path to the virtual machine folder in which to place the virtual machine, relative to the datacenter path (/<datacenter-name>/vm). For example, /dc-01/vm/foo"
  type        = string
  default     = "Discovered virtual machine"
}

variable "vsphere_network" {
  description = "The name of the network. This can be a name or path"
  type        = string
}

variable "vsphere_content_library" {
  description = "The name of the content library"
  type        = string
}

variable "vsphere_content_library_ovf" {
  description = "The name of the content library item"
  type        = string
}

# Instance Settings
variable "vm_name" {
  description = "Name of the virtual machine to create"
  type        = string
}

variable "vm_efi_secure_boot_enabled" {
  description = "Use this option to enable EFI secure boot when the firmware type is set to is efi. Default: false"
  type        = bool
  default     = true
}

# Userdata Settings
variable "user_name" {
  description = "Host ssh user name"
  type        = string
}

variable "user_passwd" {
  description = "SSH user console password"
  type        = string
}

variable "ssh_authorized_key" {
  description = "SSH Public Key for user"
  type        = string
}

variable "package_list" {
  description = "List of additional packages to install"
  type        = list(string)
  default     = []
}
