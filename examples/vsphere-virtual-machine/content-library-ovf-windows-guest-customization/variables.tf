##################################################################################
# VARIABLES
##################################################################################

# Credentials
variable "vsphere_server" {
  type        = string
  description = "The fully qualified domain name or IP address of the vCenter Server instance. (e.g. sfo-m01-vc01.sfo.rainpole.io)"
}

variable "vsphere_username" {
  type        = string
  description = "The username to login to the vCenter Server instance. (e.g. administrator@vsphere.local)"
  sensitive   = true
}

variable "vsphere_password" {
  type        = string
  description = "The password for the login to the vCenter Server instance."
  sensitive   = true
}

variable "vsphere_insecure" {
  type        = bool
  description = "Set to true for self-signed certificates."
  default     = false
}

# vSphere Settings
variable "vsphere_datacenter" {
  description = "The name of the datacenter. This can be a name or path. Can be omitted if there is only one datacenter in the inventory"
  type        = string
  default     = null
}

variable "vsphere_cluster" {
  description = "The name or absolute path to the cluster"
  type        = string
}

variable "vsphere_datastore" {
  description = "The name of the datastore. This can be a name or path"
  type        = string
}

variable "vsphere_folder" {
  description = "The path to the virtual machine folder in which to place the virtual machine, relative to the datacenter path (/<datacenter-name>/vm). For example, /dc-01/vm/foo"
  type        = string
  default     = "Discovered virtual machine"
}

variable "vsphere_network" {
  description = "The name of the network. This can be a name or path"
  type        = string
}

variable "vsphere_content_library" {
  description = "The name of the content library"
  type        = string
}

variable "vsphere_content_library_ovf" {
  description = "The name of the content library item"
  type        = string
}

# Virtual Machine Settings
variable "vm_name" {
  description = "Name of the virtual machine to create"
  type        = string
}

variable "vm_cpus" {
  description = "The number of virtual CPUs to assign to the virtual machine"
  type        = number
  default     = 2
}

variable "vm_memory" {
  description = "The size of the virtual machine memory, in MB"
  type        = number
  default     = 4096
}

variable "vm_disk_size" {
  description = "Size of the disk (in GB)"
  type        = number
  default     = 60
}

variable "vm_firmware" {
  description = "The firmware to use on the virtual machine"
  type        = string
  default     = "efi"
}

variable "vm_efi_secure_boot_enabled" {
  description = "Use this option to enable EFI secure boot when the firmware type is set to is efi. Default: false"
  type        = bool
  default     = false
}

variable "vm_ipv4_address" {
  description = "The IPv4 address assigned to the network adapter. If blank or not included, DHCP is used"
  type        = string
  default     = ""
}

variable "vm_ipv4_netmask" {
  description = "The IPv4 subnet mask, in bits (e.g. 24 for 255.255.255.0)"
  type        = number
  default     = 24
}

variable "vm_ipv4_gateway" {
  description = "The IPv4 default gateway when using network_interface customization on the virtual machine"
  type        = string
}

variable "vm_dns_suffix_list" {
  description = "A list of DNS search domains to add to the DNS configuration on the virtual machine"
  type        = list(string)
  default     = []
}

variable "vm_dns_server_list" {
  description = "The list of DNS servers to configure on the virtual machine"
  type        = list(string)
}

variable "domain" {
  description = "The domain name in which to join the virtual machine. One of this or workgroup must be included"
  type        = string
  default     = "example.com"
}

variable "domain_admin_username" {
  description = "The user account with administrative privileges to use to join the guest operating system to the domain. Required if setting join_domain"
  type        = string
  sensitive   = true
}

variable "domain_admin_password" {
  description = "The password user account with administrative privileges used to join the virtual machine to the domain. Required if setting join_domain"
  type        = string
  sensitive   = true
}

variable "vm_admin_password" {
  description = "The administrator password for the virtual machine"
  type        = string
  sensitive   = true
}
