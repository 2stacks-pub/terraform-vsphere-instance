output "datacenter_id" {
  description = "The managed object ID of this datacenter"
  value       = data.vsphere_datacenter.datacenter.id
}

output "role_privileges" {
  description = "The privileges associated with this role"
  value       = vsphere_role.packer_vsphere.role_privileges
}
