# vsphere-role

<!-- Description of module -->
This module creates a vSphere role for Hashicorp Packer

## References
<!-- Include links to external references -->
N/A

## Table of Contents
- [Requirements](#requirements)
- [Providers](#providers)
- [Modules](#modules)
- [Resources](#resources)
- [Inputs](#inputs)
- [Outputs](#outputs)

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.2.5 |
| <a name="requirement_vsphere"></a> [vsphere](#requirement\_vsphere) | >= 2.2.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_vsphere"></a> [vsphere](#provider\_vsphere) | 2.2.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [vsphere_role.packer_vsphere](https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/role) | resource |
| [vsphere_datacenter.datacenter](https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/data-sources/datacenter) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_packer_vsphere_privileges"></a> [packer\_vsphere\_privileges](#input\_packer\_vsphere\_privileges) | The vSphere privledges for the HashiCorp Packer to VMware vSphere custom role. | `list(string)` | `[]` | no |
| <a name="input_packer_vsphere_role"></a> [packer\_vsphere\_role](#input\_packer\_vsphere\_role) | The name for the HashiCorp Packer to VMware vSphere custom role. | `string` | `"Packer to vSphere Integration"` | no |
| <a name="input_vsphere_datacenter"></a> [vsphere\_datacenter](#input\_vsphere\_datacenter) | The target vSphere datacenter object name. (e.g. sfo-m01-dc01). | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_datacenter_id"></a> [datacenter\_id](#output\_datacenter\_id) | The managed object ID of this datacenter |
| <a name="output_role_privileges"></a> [role\_privileges](#output\_role\_privileges) | The privileges associated with this role |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
