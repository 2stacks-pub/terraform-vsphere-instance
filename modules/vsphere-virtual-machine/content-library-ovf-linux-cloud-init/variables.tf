##################################################################################
# VARIABLES
##################################################################################

# vSphere Settings
variable "vsphere_datacenter" {
  description = "The name of the datacenter. This can be a name or path. Can be omitted if there is only one datacenter in the inventory"
  type        = string
  default     = null
}

variable "vsphere_cluster" {
  description = "The name or absolute path to the cluster"
  type        = string
}

variable "vsphere_datastore" {
  description = "The name of the datastore. This can be a name or path"
  type        = string
}

variable "vsphere_folder" {
  description = "The path to the virtual machine folder in which to place the virtual machine, relative to the datacenter path (/<datacenter-name>/vm). For example, /dc-01/vm/foo"
  type        = string
  default     = "Discovered virtual machine"
}

variable "vsphere_network" {
  description = "The name of the network. This can be a name or path"
  type        = string
}

variable "vsphere_content_library" {
  description = "The name of the content library"
  type        = string
}

variable "vsphere_content_library_ovf" {
  description = "The name of the content library item"
  type        = string
}

# Virtual Machine Settings
variable "vm_name" {
  description = "Name of the virtual machine to create"
  type        = string
}

variable "vm_cpus" {
  description = "The number of virtual CPUs to assign to the virtual machine"
  type        = number
  default     = 2
}

variable "vm_memory" {
  description = "The size of the virtual machine memory, in MB"
  type        = number
  default     = 4096
}

variable "vm_disk_size" {
  description = "Size of the disk (in GB)"
  type        = number
  default     = 60
}

variable "vm_firmware" {
  description = "The firmware to use on the virtual machine"
  type        = string
  default     = "efi"
}

variable "vm_efi_secure_boot_enabled" {
  description = "Use this option to enable EFI secure boot when the firmware type is set to is efi. Default: false"
  type        = bool
  default     = false
}

variable "guestinfo_metadata" {
  description = "Extra metadata configuration data for the virtual machine. Must be base64 encoded."
  type        = string
}

variable "guestinfo_userdata" {
  description = "Extra userdata configuration data for the virtual machine. Must be base64 encoded."
  type        = string
}
