# content-library-ovf-windows-guest-customization

<!-- Description of module -->
This module creates a vSphere Virtual Machine from a Content Library OVF.  Instance configuration is provided using [clone](https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/virtual_machine#creating-a-virtual-machine-from-a-template), [customize](https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/virtual_machine#virtual-machine-customizations) and [windows_options](https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/virtual_machine#computer_name).

## References
<!-- Include links to external references -->
None

## Table of Contents
- [Usage](#usage)
- [Requirements](#requirements)
- [Providers](#providers)
- [Modules](#modules)
- [Resources](#resources)
- [Inputs](#inputs)
- [Outputs](#outputs)

## Usage
Provide the required variable inputs listed below.

`main.tf`
```terraform
module "instance" {
  source = "../../../modules/vsphere-virtual-machine/content-library-ovf-windows-guest-customization"

  # vSphere Settings
  vsphere_datacenter          = var.vsphere_datacenter
  vsphere_cluster             = var.vsphere_cluster
  vsphere_datastore           = var.vsphere_datastore
  vsphere_folder              = var.vsphere_folder
  vsphere_network             = var.vsphere_network
  vsphere_content_library     = var.vsphere_content_library
  vsphere_content_library_ovf = var.vsphere_content_library_ovf

  # Virtual Machines Settings
  vm_name                    = var.vm_name
  vm_cpus                    = var.vm_cpus
  vm_memory                  = var.vm_memory
  vm_disk_size               = var.vm_disk_size
  vm_firmware                = var.vm_firmware
  vm_efi_secure_boot_enabled = var.vm_efi_secure_boot_enabled
  vm_hostname                = var.vm_hostname
  vm_domain                  = var.vm_domain
  vm_ipv4_address            = var.vm_ipv4_address
  vm_ipv4_netmask            = var.vm_ipv4_netmask
  vm_ipv4_gateway            = var.vm_ipv4_gateway
  vm_dns_suffix_list         = var.vm_dns_server_list
  vm_dns_server_list         = var.vm_dns_server_list
  domain                     = var.domain
  domain_admin_username      = var.domain_admin_username
  domain_admin_password      = var.domain_admin_password
  vm_admin_password          = var.vm_admin_password
}
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.2.5 |
| <a name="requirement_vsphere"></a> [vsphere](#requirement\_vsphere) | >= 2.2.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_vsphere"></a> [vsphere](#provider\_vsphere) | 2.2.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [vsphere_virtual_machine.vm](https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/virtual_machine) | resource |
| [vsphere_compute_cluster.cluster](https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/data-sources/compute_cluster) | data source |
| [vsphere_content_library.content_library](https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/data-sources/content_library) | data source |
| [vsphere_content_library_item.content_library_item](https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/data-sources/content_library_item) | data source |
| [vsphere_datacenter.datacenter](https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/data-sources/datacenter) | data source |
| [vsphere_datastore.datastore](https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/data-sources/datastore) | data source |
| [vsphere_network.network](https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/data-sources/network) | data source |
| [vsphere_resource_pool.pool](https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/data-sources/resource_pool) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_domain"></a> [domain](#input\_domain) | The domain name in which to join the virtual machine. One of this or workgroup must be included | `string` | `"example.com"` | no |
| <a name="input_domain_admin_password"></a> [domain\_admin\_password](#input\_domain\_admin\_password) | The password user account with administrative privileges used to join the virtual machine to the domain. Required if setting join\_domain | `string` | n/a | yes |
| <a name="input_domain_admin_username"></a> [domain\_admin\_username](#input\_domain\_admin\_username) | The user account with administrative privileges to use to join the guest operating system to the domain. Required if setting join\_domain | `string` | n/a | yes |
| <a name="input_vm_admin_password"></a> [vm\_admin\_password](#input\_vm\_admin\_password) | The administrator password for the virtual machine | `string` | n/a | yes |
| <a name="input_vm_cpus"></a> [vm\_cpus](#input\_vm\_cpus) | The number of virtual CPUs to assign to the virtual machine | `number` | `2` | no |
| <a name="input_vm_disk_size"></a> [vm\_disk\_size](#input\_vm\_disk\_size) | Size of the disk (in GB) | `number` | `60` | no |
| <a name="input_vm_dns_server_list"></a> [vm\_dns\_server\_list](#input\_vm\_dns\_server\_list) | The list of DNS servers to configure on the virtual machine | `list(string)` | n/a | yes |
| <a name="input_vm_dns_suffix_list"></a> [vm\_dns\_suffix\_list](#input\_vm\_dns\_suffix\_list) | A list of DNS search domains to add to the DNS configuration on the virtual machine | `list(string)` | `[]` | no |
| <a name="input_vm_efi_secure_boot_enabled"></a> [vm\_efi\_secure\_boot\_enabled](#input\_vm\_efi\_secure\_boot\_enabled) | Use this option to enable EFI secure boot when the firmware type is set to is efi. Default: false | `bool` | `false` | no |
| <a name="input_vm_firmware"></a> [vm\_firmware](#input\_vm\_firmware) | The firmware to use on the virtual machine | `string` | `"efi"` | no |
| <a name="input_vm_ipv4_address"></a> [vm\_ipv4\_address](#input\_vm\_ipv4\_address) | The IPv4 address assigned to the network adapter. If blank or not included, DHCP is used | `string` | `""` | no |
| <a name="input_vm_ipv4_gateway"></a> [vm\_ipv4\_gateway](#input\_vm\_ipv4\_gateway) | The IPv4 default gateway when using network\_interface customization on the virtual machine | `string` | n/a | yes |
| <a name="input_vm_ipv4_netmask"></a> [vm\_ipv4\_netmask](#input\_vm\_ipv4\_netmask) | The IPv4 subnet mask, in bits (e.g. 24 for 255.255.255.0) | `number` | `24` | no |
| <a name="input_vm_memory"></a> [vm\_memory](#input\_vm\_memory) | The size of the virtual machine memory, in MB | `number` | `4096` | no |
| <a name="input_vm_name"></a> [vm\_name](#input\_vm\_name) | Name of the virtual machine to create | `string` | n/a | yes |
| <a name="input_vsphere_cluster"></a> [vsphere\_cluster](#input\_vsphere\_cluster) | The name or absolute path to the cluster | `string` | n/a | yes |
| <a name="input_vsphere_content_library"></a> [vsphere\_content\_library](#input\_vsphere\_content\_library) | The name of the content library | `string` | n/a | yes |
| <a name="input_vsphere_content_library_ovf"></a> [vsphere\_content\_library\_ovf](#input\_vsphere\_content\_library\_ovf) | The name of the content library item | `string` | n/a | yes |
| <a name="input_vsphere_datacenter"></a> [vsphere\_datacenter](#input\_vsphere\_datacenter) | The name of the datacenter. This can be a name or path. Can be omitted if there is only one datacenter in the inventory | `string` | `null` | no |
| <a name="input_vsphere_datastore"></a> [vsphere\_datastore](#input\_vsphere\_datastore) | The name of the datastore. This can be a name or path | `string` | n/a | yes |
| <a name="input_vsphere_folder"></a> [vsphere\_folder](#input\_vsphere\_folder) | The path to the virtual machine folder in which to place the virtual machine, relative to the datacenter path (/<datacenter-name>/vm). For example, /dc-01/vm/foo | `string` | `"Discovered virtual machine"` | no |
| <a name="input_vsphere_network"></a> [vsphere\_network](#input\_vsphere\_network) | The name of the network. This can be a name or path | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_vm_id"></a> [vm\_id](#output\_vm\_id) | The UUID of the virtual machine. |
| <a name="output_vm_ip_address"></a> [vm\_ip\_address](#output\_vm\_ip\_address) | The IP address selected by Terraform to be used with any provisioners configured on this resource. |
| <a name="output_vm_ip_addresses"></a> [vm\_ip\_addresses](#output\_vm\_ip\_addresses) | The current list of IP addresses on this machine, including the value of default\_ip\_address. |
| <a name="output_vm_moid"></a> [vm\_moid](#output\_vm\_moid) | The managed object reference ID of the created virtual machine. |
| <a name="output_vm_tools_status"></a> [vm\_tools\_status](#output\_vm\_tools\_status) | The state of VMware Tools in the guest. |
| <a name="output_vm_vmx_path"></a> [vm\_vmx\_path](#output\_vm\_vmx\_path) | The path of the virtual machine configuration file on the datastore in which the virtual machine is placed. |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
