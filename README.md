# terraform-vsphere-instance

This module creates Virtual machines on Vsphere using images created with [packer-examples-for-vsphere](https://github.com/vmware-samples/packer-examples-for-vsphere).  The modules listed below were created from the terraform examples [here](https://github.com/vmware-samples/packer-examples-for-vsphere/tree/main/terraform).  The goal of this repo is to modularize this example code for reusability and to provide support for [Terragrunt](https://terragrunt.io).

<!-- UPDATE THIS SECTION WHEN ADDING NEW MODULES -->
* [vsphere-role](/modules/vsphere-role): This module creates a vSphere role for Hashicorp Packer
* [content-library-ovf-linux-cloud-init](/modules/vsphere-virtual-machine/content-library-ovf-linux-cloud-init): This module creates a vSphere Virtual Machine from a Content Library OVF.  Instance configuration is provided using [cloud-init](https://cloudinit.readthedocs.io/en/latest/)
* [content-library-ovf-linux-guest-customization](/modules/vsphere-virtual-machine/content-library-ovf-linux-guest-customization): This module creates a vSphere Virtual Machine from a Content Library OVF.  Instance configuration is provided using [clone](https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/virtual_machine#creating-a-virtual-machine-from-a-template) and [customize](https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/virtual_machine#virtual-machine-customizations).
* [content-library-ovf-windows-guest-customizatio](/modules/vsphere-virtual-machine/content-library-ovf-windows-guest-customization): This module creates a vSphere Virtual Machine from a Content Library OVF.  Instance configuration is provided using [clone](https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/virtual_machine#creating-a-virtual-machine-from-a-template), [customize](https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/virtual_machine#virtual-machine-customizations) and [windows_options](https://registry.terraform.io/providers/hashicorp/vsphere/latest/docs/resources/virtual_machine#computer_name).
* [template-linux-cloud-init](/modules/vsphere-virtual-machine/template-linux-cloud-init): This module creates a vSphere Virtual Machine from an existing VM template.  Instance configuration is provided using [cloud-init](https://cloudinit.readthedocs.io/en/latest/)
* [template-linux-guest-customization](/modules/vsphere-virtual-machine/template-linux-guest-customization): Description
* [template-windows-guest-customization](/modules/vsphere-virtual-machine/template-windows-guest-customization): Description

Click on each module above to see its documentation. Head over to the [examples folder](/examples) for examples.

## Prerequisites
- Vsphere environment with Vcenter
- Templates/Content Library populated with images using [packer-examples-for-vsphere](https://github.com/vmware-samples/packer-examples-for-vsphere/blob/main/README.md)

## Contributing
Code quality and security will be validated before merge requests are accepted.

### Tools
These tools are used to ensure validation and standardization of Terraform deployments

#### Must be installed
- [pre-commit](https://github.com/gruntwork-io/pre-commit/releases)
- [terraform-docs](https://github.com/terraform-docs/terraform-docs)
- [tflint](https://github.com/terraform-linters/tflint)
- [tfsec](https://github.com/aquasecurity/tfsec)

#### Provided by Terraform
- [terraform fmt](https://www.terraform.io/docs/commands/fmt.html)
- [terraform validate](https://www.terraform.io/docs/commands/validate.html)

For more information see - [pre-commit-hooks-for-terraform](https://medium.com/slalom-build/pre-commit-hooks-for-terraform-9356ee6db882)

### To submit a merge request
```bash
git checkout -b <branch name>
pre-commit autoupdate
pre-commit run -a
git commit -a -m 'Add new feature'
git push origin <branch name>
```
Optionally run the following to automate the execution of pre-commit on every git commit.
```bash
pre-commit install
```

# License
Copyright (c) 2022

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
